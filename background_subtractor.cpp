#include <opencv2/imgcodecs.hpp> 
#include <opencv2/videoio.hpp>

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <stdio.h>
#include <iostream>
#include <sstream>
using namespace std; 
using namespace cv; 
int main() {	
	Mat frame, frame1, finalFrame; 
	VideoCapture c(0); 
	c.read(frame); 
	Mat kernel = (Mat_<float>(5, 5) << 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);
		cvtColor(frame,frame,CV_BGR2GRAY); 
	while(1) {
		char key = waitKey(60);
		if(key==27) exit(0); 
		frame.copyTo(frame1);
		c.read(frame);
		
		cvtColor(frame,frame,CV_BGR2GRAY);   
		absdiff(frame, frame1, finalFrame); 
		threshold(finalFrame, finalFrame, 15,255, THRESH_BINARY);
		erode(finalFrame,finalFrame,kernel);   
		imshow("..", finalFrame); 
	}
}
