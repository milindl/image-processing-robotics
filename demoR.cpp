#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
using namespace cv;
Mat src, grayscaled, hsv, hsvChan[3]; //s1
Mat cannified;
Mat drawnContours;
Mat awesomeFrame; 
//I will use awesome object for calibration 
//Awesome object is the centre of my video feed and the region of pixels around it
//I will use it to set HSV
int main() {
	int maxH=0, minH=179; 
	int maxS=0, minS=255; 
	int maxV=0, minV=255; 
	//Step 0: get input, set up loop
	VideoCapture cam(0); 
	//first i gotta get awesomeframe
	char key;
	while(key!=27) {
		cam.read(src);
		
	rectangle(src, Rect((src.rows/2),(src.cols/2),100,100) , Scalar(0,0,0));
		key=waitKey(33); 
		imshow("Original", src); 	
	}
	destroyAllWindows(); 
	cam.read(awesomeFrame); 
	imshow("AwesomeFrame", awesomeFrame); 
	waitKey(0);
	Mat cen(awesomeFrame, Rect((src.rows/2),(src.cols/2),100,100));
	blur(cen,cen,Size(9,9)); 
	destroyAllWindows(); 
	//imshow("area", cen); 	
	//need to find RANGE of colors now!!!! Range of H,S and V to be precise
	cvtColor(cen, cen, CV_BGR2HSV); 
	Point3_<uchar>* p=cen.ptr<Point3_<uchar> >(cen.rows/2,cen.cols/2);
	Point3_<uchar>* pix; 
	for(int i=0; i<cen.rows; i++) {
		for(int j=0; j<cen.cols; j++) {
			pix = cen.ptr<Point3_<uchar> >(i,j); 
			//cout<<" "<<((int)pix->x)<<endl; 
			if((pix->x) > maxH) maxH = pix->x; 
			if((pix->x) < minH) minH = pix->x; 
			if((pix->y) > maxS) maxS = pix->y; 
			if((pix->y) < minS) minS = pix->y; 
			if((pix->z) > maxV) maxV = pix->z; 
			if((pix->z) < minV) minV = pix->z; 
		}
	}
	if(maxH-minH<20) {
		if(maxH<169)maxH+=5;
		if(minH>10) minH-=5; 
	}

	if(maxS-minS<40) {
		if(maxS+20<255)maxS+=20;
		if(minS-20>0) minS-=20; 
	}

	if(maxV-minV<40) {
		if(maxV+20<255)maxV+=20;
		if(minV-20>0) minV==20; 
	}
	cerr<<" "<<minH<<" "<<maxH<<endl; 
		while(1) {
		char key = waitKey(33); 
		if(key==27) return 0; 
		cam.read(src); 
		flip(src,src,1);
		cvtColor(src,grayscaled,CV_BGR2GRAY);
		cvtColor(src,hsv,CV_BGR2HSV);
			

		//Step 1: Detect contours AS Areas
		//Canny(grayscaled, cannified, 100, 200, 3); 
		inRange(hsv, Scalar(minH, minS, minV), Scalar(maxH, maxS, maxV), cannified); 
		medianBlur(cannified, cannified, 15); 		
		vector<vector<Point> > contours; 

		vector<vector<Point> > hull; 		vector<Vec4i> hierarchy;
		findContours(cannified, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );	
		findContours(cannified, hull, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );	
	
		vector<Moments> mu(contours.size() );
	
  		for( int i = 0; i < contours.size(); i++ ) {
			mu[i] = moments( contours[i], false );
		}
				
		vector<Point2f> mc( contours.size() );
		for( int i = 0; i < contours.size(); i++ ) {
			mc[i] = Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 ); 
		}
		for( int i=0; i<contours.size(); i++) {
			//cerr<<"Reached "<<contours[i].size()<<endl; 
			 
			convexHull(contours[i],hull[i], true);	
		}
  
		Mat drawing = Mat::zeros( cannified.size(), CV_8UC3 );		Mat drawingHull = Mat::zeros( cannified.size(), CV_8UC3 );
  		for( int i = 0; i< contours.size(); i++ )
     		{	
			if(mu[i].m00<1000) continue; 
			
       			Scalar color = Scalar(255,255,255);
       			drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, Point() );
			drawContours( drawingHull, hull, i, color, 2, 8, hierarchy, 0, Point() );
       			//circle( drawing, mc[i], 4, color, -1, 8, 0 );
     		}

		//Step 2: find ones which have a reasonable area
		//Step 3: Calculate <h>, <s>, <v> 
		//Display <h> if <v> and <s> satisfy a condition
		//Outputs:
		imshow("Original", src); 
		imshow("Contours", drawing+src); 
		imshow("Hulls", drawingHull+src); 
		//imshow("HSV", hsv);  
		//imshow("Cannified", cannified); 
	}	
	return 0; 
}
